package com.example.intenbaru;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class pindahdata extends AppCompatActivity {
    public static final String Nama = "nama";
    public static final String Umur = "umur";

    TextView tvData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pindahdata);

        tvData = findViewById(R.id.data);
        String name = getIntent().getStringExtra(Nama);
        int umur = getIntent().getIntExtra(Umur,0);

        String text = "Nama :" + name + " \nUmur :" + umur;
        tvData.setText(text);

    }
}
