package com.example.intenbaru;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnMoveActivity;
    Button btnMoveActivityData;
    Button btnMoveActivityObject;
    Button btnDialNumber;
    Button btnMoveForResult;
    TextView tvResult;

    private int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMoveActivity = findViewById(R.id.Pindah_Aktivity);
        btnMoveActivity.setOnClickListener(this);

        btnMoveActivityData = findViewById(R.id.Pindah_Aktivity_Data);
        btnMoveActivityData.setOnClickListener(this);

        btnMoveActivityObject = findViewById(R.id.Pindah_Aktivity_Objek);
        btnMoveActivityObject.setOnClickListener(this);

        btnDialNumber = findViewById(R.id.btn_dial_number);
        btnDialNumber.setOnClickListener(this);

        btnMoveForResult = findViewById(R.id.move_for_result);
        btnMoveForResult.setOnClickListener(this);


        tvResult = findViewById(R.id.tv_result);



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.Pindah_Aktivity:
                Intent moveIntent = new Intent(MainActivity.this, MoveActivity.class);
                startActivity(moveIntent);
                break;
            case R.id.Pindah_Aktivity_Data:
                Intent PindahData = new Intent(MainActivity.this, pindahdata.class);
                PindahData.putExtra(pindahdata.Nama, "Hasan Zain");
                PindahData.putExtra(pindahdata.Umur, 21);
                startActivity(PindahData);
                break;
            case R.id.Pindah_Aktivity_Objek:
                Person person = new Person();
                person.setName("Hasan Zain");
                person.setAge(22);
                person.setEmail("hasanzain@mail");
                person.setCity("Trenggalek");

                Intent moveWithObject = new Intent(MainActivity.this,MoveWithObject.class);
                moveWithObject.putExtra(MoveWithObject.EXTRA_PERSON,person);
                startActivity(moveWithObject);
                break;

            case R.id.btn_dial_number:
                String phoneNumber = "088227044212";
                Intent dialPhoneIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(("tel:" + phoneNumber)));
                startActivity(dialPhoneIntent);
                break;
            case R.id.move_for_result:
                Intent moveForResultIntent = new Intent(MainActivity.this,MoveForResult.class);
                startActivityForResult(moveForResultIntent, REQUEST_CODE);
                break;

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == MoveForResult.RESULT_CODE) {
                int selectedValue = data.getIntExtra(MoveForResult.EXTRA_SELECTED_VALUE, 0);
                tvResult.setText(String.format("Hasil : %s", selectedValue));
            }
        }
    }
}
